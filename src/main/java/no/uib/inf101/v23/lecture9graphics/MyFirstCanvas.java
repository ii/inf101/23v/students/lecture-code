package no.uib.inf101.v23.lecture9graphics;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;

public class MyFirstCanvas extends JPanel {

  public MyFirstCanvas() {
    this.setPreferredSize(new Dimension(220, 100));
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;

    fillCenteredCircle(g2, 50, 50, 30, Color.DARK_GRAY);
    fillCenteredCircle(g2, 50, 50, 20, Color.GRAY);
    fillCenteredCircle(g2, 50, 50, 10, Color.LIGHT_GRAY);
    fillCenteredCircle(g2, 50, 50, 2, Color.WHITE);

    drawMan(g2, 120, 10, 30, 80);
    drawMan(g2, 170, 20, 30, 70);
  }

  private void fillCenteredCircle(Graphics2D g2, double cx, double cy, double r, Color color) {
    Ellipse2D circle = new Ellipse2D.Double(cx - r, cy - r, 2*r, 2*r);
    g2.setColor(color);
    g2.fill(circle);
  }

  private void drawMan(Graphics2D g2, double xLeft, double yTop, double w, double h) {
    // Midlertidig: området som skal tegnes på
    // g2.setColor(Color.decode("#bbbbff"));
    // g2.fill(new Rectangle2D.Double(xLeft, yTop, w, h));

    // Variabler
    double xMid = xLeft + w/2;
    double xRight = xLeft + w;
    double yShoulder = yTop + h/3;
    double yHand = yTop + h/2;
    double yWaist = yTop + 2*h/3;
    double yFeet = yTop + h;

    // Tegne hodet
    Ellipse2D head = new Ellipse2D.Double(xLeft, yTop, w, yShoulder - yTop);
    g2.setColor(Color.YELLOW);
    g2.fill(head);
    g2.setColor(Color.BLACK);
    g2.draw(head);

    // Tegne kroppen
    Line2D body = new Line2D.Double(xMid, yShoulder, xMid, yWaist);
    g2.draw(body);

    // Tegne beina
    g2.draw(new Line2D.Double(xMid, yWaist, xLeft, yFeet));
    g2.draw(new Line2D.Double(xMid, yWaist, xRight, yFeet));

    // Tegne armene
    g2.draw((new Line2D.Double(xMid, yShoulder, xLeft, yHand)));
    g2.draw((new Line2D.Double(xMid, yShoulder, xRight, yHand)));

  }
}
