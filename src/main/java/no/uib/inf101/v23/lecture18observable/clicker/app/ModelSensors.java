package no.uib.inf101.v23.lecture18observable.clicker.app;


import no.uib.inf101.v23.lecture18observable.observable.ControlledObservableValue;
import no.uib.inf101.v23.lecture18observable.observable.ObservableValue;

public class ModelSensors {

  private final ControlledObservableValue<Boolean> sensorA = new ControlledObservableValue<>(false);
  private final ControlledObservableValue<Boolean> sensorB = new ControlledObservableValue<>(false);

  public ObservableValue<Boolean> getSensorA() {
    return this.sensorA;
  }

  public ObservableValue<Boolean> getSensorB() {
    return this.sensorB;
  }

  public void setSensorA(boolean b) {
    this.sensorA.setValue(b);
  }

  public void setSensorB(boolean b) {
    this.sensorB.setValue(b);
  }
}
