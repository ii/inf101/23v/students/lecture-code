package no.uib.inf101.v23.lecture18observable.sampleuse;

public class Foo {
  private int x = 0;

  public Foo getModifiedCopy() {
    Foo copy = new Foo();
    copy.x = this.x + 1;
    return copy;
  }

  @Override
  public String toString() {
    return "Foo[x=" + this.x + "]";
  }
}
