package no.uib.inf101.v23.lecture18observable.clicker.app;

import no.uib.inf101.v23.lecture18observable.observable.ObservableValue;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

public class ViewSensor extends JPanel {

  private final ObservableValue<Boolean> model;

  public ViewSensor(ObservableValue<Boolean> model) {
    this.model = model;
    this.model.addChangeListener((src, nVal, oVal) -> this.repaint());
    this.setPreferredSize(new Dimension(300, 300));
  }

  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;

    Ellipse2D circle = new Ellipse2D.Double(0, 0, this.getWidth(), this.getHeight());
    Color color = this.model.getValue() ? Color.RED : Color.BLACK;
    g2.setColor(color);
    g2.fill(circle);
  }
}
