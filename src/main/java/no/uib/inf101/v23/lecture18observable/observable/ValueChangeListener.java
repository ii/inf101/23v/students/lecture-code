package no.uib.inf101.v23.lecture18observable.observable;

@FunctionalInterface
public interface ValueChangeListener<E> {
    void onValueChanged(ObservableValue<E> source, E newValue, E oldValue);
}
