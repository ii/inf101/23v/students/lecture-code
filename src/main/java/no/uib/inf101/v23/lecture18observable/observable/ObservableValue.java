package no.uib.inf101.v23.lecture18observable.observable;

public interface ObservableValue<E> {
    E getValue();
    void addChangeListener(ValueChangeListener<E> listener);
}
