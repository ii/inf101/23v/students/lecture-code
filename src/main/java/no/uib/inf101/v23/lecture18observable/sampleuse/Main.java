package no.uib.inf101.v23.lecture18observable.sampleuse;

public class Main {
  public static void main(String[] args) {
    ClientProducer producer = new ClientProducer();
    ClientConsumer consumer = new ClientConsumer(producer);

    producer.changeStuff();
    producer.changeStuff();
    producer.changeStuff();
  }
}
