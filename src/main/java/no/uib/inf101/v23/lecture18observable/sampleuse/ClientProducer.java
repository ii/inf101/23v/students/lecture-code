package no.uib.inf101.v23.lecture18observable.sampleuse;


import no.uib.inf101.v23.lecture18observable.observable.ControlledObservableValue;
import no.uib.inf101.v23.lecture18observable.observable.ObservableValue;

public class ClientProducer {

  private final ControlledObservableValue<Foo> variable = new ControlledObservableValue<>(new Foo());

  public void changeStuff() {
    Foo currentValue = this.variable.getValue();
    Foo nextValue = currentValue.getModifiedCopy();
    this.variable.setValue(nextValue);
  }

  public ObservableValue<Foo> getVariable() {
    return this.variable;
  }
}
