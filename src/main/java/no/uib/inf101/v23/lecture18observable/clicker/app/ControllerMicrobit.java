package no.uib.inf101.v23.lecture18observable.clicker.app;

import no.uib.inf101.v23.lecture18observable.clicker.serialcom.SerialComListener;

public class ControllerMicrobit {

  private final ModelSensors model;

  public ControllerMicrobit(ModelSensors model) {
    this.model = model;
    SerialComListener comListener = new SerialComListener(this::foo);
    comListener.start();
  }

  private void foo(char symbol) {
    System.out.println("Received " + symbol);
    if (symbol == 'A') {
      this.model.setSensorA(true);
    } else if (symbol == 'a') {
      this.model.setSensorA(false);
    } else if (symbol == 'B') {
      this.model.setSensorB(true);
    } else if (symbol == 'b') {
      this.model.setSensorB(false);
    }
  }
}
