package no.uib.inf101.v23.lecture18observable.observable;

import java.util.ArrayList;
import java.util.List;

public class ControlledObservableValue<E> implements ObservableValue<E> {

  private E value;
  private final List<ValueChangeListener<E>> listeners = new ArrayList<>();

  public ControlledObservableValue(E initialValue) {
    this.value = initialValue;
  }

  @Override
  public E getValue() {
    return this.value;
  }

  @Override
  public void addChangeListener(ValueChangeListener<E> listener) {
    this.listeners.add(listener);
  }

  public void setValue(E newValue) {
    E oldValue = this.value;
    this.value = newValue;
    for (ValueChangeListener<E> listener : this.listeners) {
      listener.onValueChanged(this, newValue, oldValue);
    }
  }
}
