package no.uib.inf101.v23.lecture18observable.sampleuse;

import no.uib.inf101.v23.lecture18observable.observable.ObservableValue;

public class ClientConsumer {

  private final ObservableValue<Foo> variable;

  public ClientConsumer(ClientProducer producer) {
    this.variable = producer.getVariable();
    variable.addChangeListener(this::listener);
  }

  private void listener(ObservableValue<Foo> source, Foo newValue, Foo oldValue) {
    System.out.println("Value changed from " + oldValue + " to " + newValue);
  }

}
