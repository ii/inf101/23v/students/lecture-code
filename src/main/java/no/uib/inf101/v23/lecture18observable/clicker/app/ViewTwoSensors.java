package no.uib.inf101.v23.lecture18observable.clicker.app;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

public class ViewTwoSensors extends JPanel {

  public ViewTwoSensors(ModelSensors model) {
    this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
    this.add(new ViewSensor(model.getSensorA()));
    this.add(new ViewSensor(model.getSensorB()));
  }
}
