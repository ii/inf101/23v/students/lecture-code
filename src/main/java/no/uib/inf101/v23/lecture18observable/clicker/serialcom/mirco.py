# This code is written in micropython (not to be confused with MakeCode python)
# To upload the code to a microbit, copy the code into the editor at
# https://python.microbit.org/v/2 and flash it after connecting the micro:bit.

from microbit import *

aPressed = button_a.is_pressed()
bPressed = button_b.is_pressed()

def send(msg):
    print(msg, end='')

def setA(newValue):
    global aPressed
    if aPressed == newValue: return
    aPressed = newValue
    display.set_pixel(0, 2, 9 if aPressed else 0)
    send('A' if aPressed else 'a')

def setB(newValue):
    global bPressed
    if bPressed == newValue: return
    bPressed = newValue
    display.set_pixel(4, 2, 9 if bPressed else 0)
    send('B' if bPressed else 'b')

send('A' if aPressed else 'a')
send('B' if bPressed else 'b')
display.clear()
while True:
    setA(button_a.is_pressed())
    setB(button_b.is_pressed())
