package no.uib.inf101.v23.lecture18observable.clicker.app;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main {
  public static void main(String[] args) {
    ModelSensors model = new ModelSensors();
    JPanel view = new ViewTwoSensors(model);
    new ControllerMicrobit(model);

    JFrame frame = new JFrame();
    frame.setTitle("INF101");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setContentPane(view);
    frame.pack();
    frame.setVisible(true);
  }
}
