package no.uib.inf101.v23.lecture16eventbus.model;

public record Person(String name, int age) { }
