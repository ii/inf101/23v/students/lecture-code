package no.uib.inf101.v23.lecture16eventbus.view;

import no.uib.inf101.v23.lecture16eventbus.model.Person;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


public class ViewPerson {

  //////////////////////////////////////////////
  ////            TEST CODE                 ////
  //// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv ////
  public static void main(String[] args) {
    Person adam = new Person("Adam", 20);
    ViewPerson view = new ViewPerson(adam);
    
    JFrame frame = new JFrame("MOCK");
    frame.setContentPane(view.mainPanel);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
  }
  //// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ ////
  ////           /TEST CODE                 ////
  //////////////////////////////////////////////
  
  private JPanel mainPanel;
  
  public ViewPerson(Person p) {
    this.mainPanel = new JPanel();
    this.mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
    this.mainPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
    
    JLabel nameLabel = new JLabel("Name: " + p.name());
    JLabel ageLabel = new JLabel("Age: " + p.age());
    
    this.mainPanel.add(nameLabel);
    this.mainPanel.add(ageLabel);
  }
  
  public JPanel getMainPanel() {
    return this.mainPanel;
  }
}
