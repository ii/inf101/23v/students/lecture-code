package no.uib.inf101.v23.lecture16eventbus.eventbus;

@FunctionalInterface
public interface EventHandler {
  void handle(Event event);
}
