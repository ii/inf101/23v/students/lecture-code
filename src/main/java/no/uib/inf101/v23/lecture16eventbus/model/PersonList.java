package no.uib.inf101.v23.lecture16eventbus.model;

import java.util.ArrayList;
import java.util.List;

public class PersonList {
  
  private List<Person> persons = new ArrayList<>();
  
  public Iterable<Person> getPersons() {
    return this.persons;
  }
  
  public void addPerson(Person person) {
    this.persons.add(person);
  }
}
