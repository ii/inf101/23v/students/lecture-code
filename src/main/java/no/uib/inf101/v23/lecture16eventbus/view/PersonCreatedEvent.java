package no.uib.inf101.v23.lecture16eventbus.view;

import no.uib.inf101.v23.lecture16eventbus.eventbus.Event;

public record PersonCreatedEvent(String name, int age) implements Event {
}
