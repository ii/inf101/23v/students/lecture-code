package no.uib.inf101.v23.lecture16eventbus.view;

import java.awt.BorderLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import no.uib.inf101.v23.lecture16eventbus.eventbus.Event;
import no.uib.inf101.v23.lecture16eventbus.eventbus.EventBus;
import no.uib.inf101.v23.lecture16eventbus.model.Person;
import no.uib.inf101.v23.lecture16eventbus.model.PersonList;

public class ViewMain {

  //////////////////////////////////////////////
  ////            TEST CODE                 ////
  //// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv ////
  public static void main(String[] args) {
    EventBus eventBus = new EventBus();
    PersonList model = new PersonList();
    for (int i = 0; i < 10; i++) {
      model.addPerson(new Person("Adam", i));
      model.addPerson(new Person("Eva", i));
    }
    
    ViewMain view = new ViewMain(model, eventBus);
    
    JFrame frame = new JFrame("MOCK");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setContentPane(view.mainPanel);
    frame.pack();
    frame.setVisible(true);
  }
  //// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ ////
  ////           /TEST CODE                 ////
  //////////////////////////////////////////////
  
  private PersonList model;
  private JPanel mainPanel;
  private ViewListOfPeople viewListOfPeople;
  private ViewPerson viewPerson;
  private JPanel rightPanel;
  private EventBus eventBus;
  private JPanel leftPanel;
  
  public ViewMain(PersonList model, EventBus eventBus) {
    this.eventBus = eventBus;
    eventBus.register(this::switchPersonToView);
    this.model = model;
    
    this.mainPanel = new JPanel();
    this.mainPanel.setLayout(new BorderLayout());
    this.viewListOfPeople = new ViewListOfPeople(model, eventBus);
    this.viewPerson = new ViewPerson(new Person("Foo", -1));
    
    this.rightPanel = new JPanel();
    this.leftPanel = new JPanel();
    this.mainPanel.add(this.leftPanel, BorderLayout.WEST);
    this.mainPanel.add(this.rightPanel, BorderLayout.CENTER);
    
    this.setupLeftPanel();
    
    this.rightPanel.add(viewPerson.getMainPanel());
  }
  
  private void setupLeftPanel() {
    this.leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.PAGE_AXIS));
    JButton button = new JButton("+");
    button.addActionListener(e -> {
      switchToCreatePersonView();
    });
    this.leftPanel.add(button);
    this.leftPanel.add(viewListOfPeople.getMainPanel());
  }
  
  private void switchToCreatePersonView() {
    this.rightPanel.removeAll();
    ViewCreatePerson view = new ViewCreatePerson(this.eventBus);
    this.rightPanel.add(view.getMainPanel());
    this.mainPanel.revalidate();
    this.mainPanel.repaint();
  }
  
  private void switchPersonToView(Event event) {
    if (event instanceof PersonClickedEvent personClickedEvent) {
      this.rightPanel.removeAll();
      this.viewPerson = new ViewPerson(personClickedEvent.personClicked());
      this.rightPanel.add(viewPerson.getMainPanel());
      this.mainPanel.revalidate();
      this.mainPanel.repaint();
    }
  }

  public JPanel getMainPanel() {
    return this.mainPanel;
  }
}
