package no.uib.inf101.v23.lecture16eventbus.view;

import no.uib.inf101.v23.lecture16eventbus.eventbus.EventBus;

import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class ViewCreatePerson {

    //////////////////////////////////////////////
    ////            TEST CODE                 ////
    //// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv ////
    public static void main(String[] args) {
        EventBus eventBus = new EventBus();
        ViewCreatePerson view = new ViewCreatePerson(eventBus);
        eventBus.register(e -> {
            if (e instanceof PersonCreatedEvent event) {
                System.out.println(event);
            }
        });

        JFrame frame = new JFrame("MOCK");
        frame.setContentPane(view.mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
    //// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ ////
    ////           /TEST CODE                 ////
    //////////////////////////////////////////////
    
    private EventBus eventBus;
    private JPanel mainPanel;

    public ViewCreatePerson(EventBus eventBus) {
        this.eventBus = eventBus;


        this.mainPanel = new JPanel();
        this.mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));

        this.mainPanel.add(new JLabel("Name:"));

        JTextField nameField = new JTextField();
        this.mainPanel.add(nameField);

        this.mainPanel.add(new JLabel("Age:"));

        JTextField ageField = new JTextField();
        this.mainPanel.add(ageField);

        JButton createButton = new JButton("Create");
        this.mainPanel.add(createButton);

        ActionListener listener = e -> {
            String name = nameField.getText();
            int age = Integer.parseInt(ageField.getText());
            this.eventBus.post(new PersonCreatedEvent(name, age));
        };
        createButton.addActionListener(listener);
    }

    public JPanel getMainPanel() {
        return this.mainPanel;
    }
}
