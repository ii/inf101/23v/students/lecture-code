package no.uib.inf101.v23.oopConcepts.list;

import java.util.ArrayList;

public class List<T> {
    
    private ArrayList<T> list;

    public List() {
        this.list = new ArrayList<>();
    }

    public void add(T elem) {
        list.add(elem);
    }

    public void add(int index, T elem) {
        list.add(index, elem);
    }
}
