package no.uib.inf101.v23.oopConcepts.book;

public class Page {
    
    /**
     * Max number of characters on a single page
     */
    private static final int MAX_TEXT_SIZE = 3000;
    /**
     * Text on page
     */
    private String text;

    public Page(String text) {
        if (text.length() > MAX_TEXT_SIZE)
            throw new IllegalArgumentException("Too much text");

        this.text = text;
    }

    public String getText() {
        return text;
    }

}
