package no.uib.inf101.v23.oopConcepts.person;

public class Cat {
    
    private String name;
    private String breed;

    public Cat(String name, String breed) {
        this.name = name;
        this.breed = breed;
    }
    
}
