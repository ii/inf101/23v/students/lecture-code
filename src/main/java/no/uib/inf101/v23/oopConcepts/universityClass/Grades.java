package no.uib.inf101.v23.oopConcepts.universityClass;

import java.util.ArrayList;
import java.util.List;

/**
 * This class holds a list of grades and a value for grade point average.
 * Grade point average (GPA) is a decimal value, which is the average of all letter grades represented by integer values. 
 * If you have the grades: A, E, B, B, C your GPA is (5 + 1 + 4 + 4 + 3) / 5 = 3.4
 * If you have the grades: E, D, E, B, F (FAIL) your GPA is (1 + 2 + 1 + 4) / 4 = 2.0. Note that F is not included in the calculation of GPA.
 * If you have the grades: F, F, F, F, F your GPA is 0.0.
 */
public class Grades {
    
    private double GPA = 0.0;
    private List<Grade> grades;

    public Grades() {
        grades = new ArrayList<>();
    }

    public Grades(List<Grade> grades) {
        this.grades = grades;
        calculateGPA();
    }

    /**
     * Calculates the grade point average of all grades in <code>grades</code>.
     * The grade FAIL should not be included in the calculation.
     */
    private void calculateGPA() {
        double sum = 0;
        int nNonFail = 0;
        for (Grade grade : grades) {
            if (grade == Grade.FAIL)
                continue;
            sum += grade.numberRepresentation;
            nNonFail += 1;
        }
        if (nNonFail == 0)
            this.GPA = 0.0;
        else
            this.GPA = sum / nNonFail;
    }

    public void add(Grade grade) {
        grades.add(grade);
        calculateGPA();
    }

    public double getGPA() {
        return GPA;
    }
}
