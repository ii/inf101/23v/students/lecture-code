package no.uib.inf101.v23.oopConcepts.cardDeck;

import java.util.Arrays;
import java.util.List;

/**
 * Ranks of conventional card deck, 
 * such as King, Queen, two, ace, etc.
 */
public enum CardRank {
    
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5),
    SIX(6),
    SEVEN(7),
    EIGHT(8),
    NINE(9),
    TEN(10),
    JACK(11),
    QUEEN(12),
    KING(13),
    ACE(14);

    public static final List<CardRank> CARD_RANKS = Arrays.asList(CardRank.values());

    /**
     * Integer value representing the card type.
     * All numbered cards have that integer value as <code>cardNumber</code>,
     * while the pictured cards have the following integer values after 10.
     */
    public final int cardNumber;

    private CardRank(int cardNumber) {
        this.cardNumber = cardNumber;
    }
    
}
