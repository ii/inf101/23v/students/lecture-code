package no.uib.inf101.v23.oopConcepts.cardDeck;

import java.util.ArrayList;

public class CardDeckInheritance extends ArrayList<Card> {

    public static CardDeckInheritance createFullDeck() {
        CardDeckInheritance deck = new CardDeckInheritance();
        for (CardRank rank : CardRank.CARD_RANKS) {
            for (CardSuite suite : CardSuite.CARD_SUITS) {
                Card card = new Card(rank, suite);
                deck.add(card);
            }
        }
        return deck;
    }

    @Override
    public boolean add(Card card) {
        if (size() >= 52) {
            return false;
        }
        return super.add(card);
    }

}
