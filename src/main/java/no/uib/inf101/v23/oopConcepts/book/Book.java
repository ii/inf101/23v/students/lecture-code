package no.uib.inf101.v23.oopConcepts.book;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Book implements Iterable<Page> {
   
    private List<Page> pages;

    public Book() {
        pages = new ArrayList<>();
    }

    public Book(List<Page> pages) {
        this.pages = pages;
    }

    public int length() {
        return pages.size();
    }

    public Page getPage(int pageNumber) {
        if (pageNumber >= length())
            throw new IndexOutOfBoundsException("Not that many pages in the book");

        return pages.get(pageNumber);
    }

    @Override
    public Iterator<Page> iterator() {
        return pages.iterator();
    }
}
