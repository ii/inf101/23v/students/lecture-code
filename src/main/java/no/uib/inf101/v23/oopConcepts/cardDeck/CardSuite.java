package no.uib.inf101.v23.oopConcepts.cardDeck;

import java.awt.Color;
import java.util.Arrays;
import java.util.List;

/**
 * Suite of conventional card deck:
 * Clubs, Diamonds, Hearts and Spades. 
 */
public enum CardSuite {

    CLUBS(0, Color.BLACK),
    DIAMONDS(1, Color.RED),
    HEARTS(2, Color.RED),
    SPADES(3, Color.BLACK);
    
    public static final List<CardSuite> CARD_SUITS = Arrays.asList(CardSuite.values());

    public final int suiteNumber;
    public final Color color;

    private CardSuite(int suiteNumber, Color color) {
        this.suiteNumber = suiteNumber;
        this.color = color;
    }
 
}
