package no.uib.inf101.v23.oopConcepts.cardDeck;

/**
 * Single card in a conventional card deck.
 * A card has a rank and suite. 
 * For instance, rank: Queen, suite: Diamonds.
 */
public class Card implements Comparable<Card> {

    public final CardRank rank;
    public final CardSuite suite;

    public Card(CardRank rank, CardSuite suite) {
        this.rank = rank;
        this.suite = suite;
    }

    @Override
    public int compareTo(Card other) {
        return Integer.compare(this.rank.cardNumber, other.rank.cardNumber);
    }

    @Override
    public String toString() {
        return rank + " of " + suite;
    }
    
}
