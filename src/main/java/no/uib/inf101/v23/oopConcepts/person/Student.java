package no.uib.inf101.v23.oopConcepts.person;

public class Student extends Person {

    private int studentNumber;

    public Student(String name, int yearOfBirth, int studentNumber) {
        super(name, yearOfBirth);
        this.studentNumber = studentNumber;
    }

    public int getStudentNumber() {
        return studentNumber;
    }
    
}
