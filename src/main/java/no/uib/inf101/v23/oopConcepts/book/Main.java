package no.uib.inf101.v23.oopConcepts.book;

import java.util.Arrays;

public class Main {
  
    public static void main(String[] args) {
        Book haroldPoter = new Book(Arrays.asList(
            new Page("Why doesn't Voldemort wear glasses?"),
            new Page("Nobody nose")
        ));

        for (Page page : haroldPoter) {
            String text = page.getText();
            System.out.println(text);
        }
    }
}
