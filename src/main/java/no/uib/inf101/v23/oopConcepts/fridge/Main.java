package no.uib.inf101.v23.oopConcepts.fridge;

public class Main {
    
    public static void main(String[] args) {
        IFridge fridge = new EasyFridge();
        cleanFridge(fridge);
    }

    public static void cleanFridge(IFridge fridge) {
        fridge.emptyFridge();
    }
    
}
