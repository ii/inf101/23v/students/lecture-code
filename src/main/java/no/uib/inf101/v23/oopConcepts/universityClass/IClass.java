package no.uib.inf101.v23.oopConcepts.universityClass;

/**
 * Interface for datatype representing an university class of students.
 */
public interface IClass extends Iterable<Student> {
    
    /**
     * Get the average GPA of all students in the class.
     * @return average GPA of all students
     */
    public double getClassGPA();

    /**
     * Get the number of students in the class.
     * @return number of students in the class
     */
    public int numberOfStudents();

    /**
     * Get student with ID <code>ID</code>.
     * @param ID
     * @throws IllegalArgumentException if there is no student with the ID <code>ID</code>.
     * @return student with <code>ID</code>
     */
    public Student getStudent(int ID);

}
