package no.uib.inf101.v23.oopConcepts.cardDeck;

import java.util.ArrayList;

public class CardDeckComposition {
    
    private ArrayList<Card> deck;

    public CardDeckComposition() {
        deck = new ArrayList<>();
    }

    public boolean add(Card card) {
        if (deck.size() >= 52) {
            return false;
        }
        return deck.add(card);
    }
























    
    public static CardDeckComposition createFullDeck() {
        CardDeckComposition deck = new CardDeckComposition();
        for (CardRank rank : CardRank.CARD_RANKS) {
            for (CardSuite suite : CardSuite.CARD_SUITS) {
                Card card = new Card(rank, suite);
                deck.add(card);
            }
        }
        return deck;
    }

}
