package no.uib.inf101.v23.oopConcepts.universityClass;

import java.util.List;

public class Student {
    
    public final int ID;
    public final String name;
    private Grades grades;

    public Student(int ID, String name) {
        this.ID = ID;
        this.name = name;
        grades = new Grades();
    }

    public Student(int ID, String name, List<Grade> gradeList) {
        this.ID = ID;
        this.name = name;
        this.grades = new Grades(gradeList);
    }

    public Grades getGrades() {
        return grades;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Student) {
			return this.ID == ((Student) obj).ID;
		}
		return false;
    }
}
