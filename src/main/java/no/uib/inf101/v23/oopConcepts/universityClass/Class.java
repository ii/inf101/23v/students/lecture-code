package no.uib.inf101.v23.oopConcepts.universityClass;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Datatype representing an university class of students.
 */
public class Class implements IClass {
    
    private List<Student> students;

    public Class() {
        students = new ArrayList<>();
    }

    @Override
    public double getClassGPA() {
        double classGPA = 0;
        for (Student student : students) {
            classGPA += student.getGrades().getGPA();
        }
        return classGPA / students.size();
    }

    @Override
    public int numberOfStudents() {
        return students.size();
    }

    @Override
    public Student getStudent(int ID) {
        for (Student student : students) {
            if (student.ID == ID)
                return student;
        }
        throw new IllegalArgumentException("No student with ID: " + ID);
    }

    @Override
    public Iterator<Student> iterator() {
        return students.iterator();
    }
}
