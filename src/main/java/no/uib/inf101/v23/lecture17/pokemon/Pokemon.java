package no.uib.inf101.v23.lecture17.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon implements IPokemon {

	String name;
	int healthPoints;
	int maxHealthPoints;
	int strength;
	Random random;

	public Pokemon(String name) {
		if (name == null)
			throw new IllegalArgumentException("Name cannot be null");
		if (name.isEmpty())
			throw new IllegalArgumentException("A Pokemon must have non empty name");
		
		this.name = name;
		this.random = new Random();
		this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
		this.maxHealthPoints = this.healthPoints;
		this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getStrength() {
		return strength;
	}

	@Override
	public int getCurrentHP() {
		return healthPoints;
	}

	@Override
	public int getMaxHP() {
		return maxHealthPoints;
	}

	public boolean isAlive() {
		return this.healthPoints > 0;
	}

	@Override
	public void attack(IPokemon target) {
		int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());
		if (damageInflicted < 0) {
			damageInflicted = 0;
		}
		System.out.printf("%s attacks %s.%n", this.name, target.getName());
		target.damage(damageInflicted);

		if (!target.isAlive()) {
			System.out.printf("%s is defeated by %s.%n", target.getName(), this.getName());
		}
	}

	@Override
	public void damage(int damageTaken) {
		if (damageTaken < 0)
			return;
		this.healthPoints -= damageTaken;
		if (this.healthPoints < 0) {
			this.healthPoints = 0;
		}
		System.out.printf("%s takes %d damage and is left with %d/%d HP%n",
				this.getName(), damageTaken, this.healthPoints, this.maxHealthPoints);
	}

	public String toString() {
		StringBuilder strBuild = new StringBuilder();
		strBuild.append(getName());
		strBuild.append(" HP: (" + healthPoints + "/" + maxHealthPoints + ")");
		strBuild.append(" STR: " + strength);
		return strBuild.toString();
	}

}

