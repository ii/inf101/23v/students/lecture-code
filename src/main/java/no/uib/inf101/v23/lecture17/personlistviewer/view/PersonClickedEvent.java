package no.uib.inf101.v23.lecture17.personlistviewer.view;

import no.uib.inf101.v23.lecture17.personlistviewer.eventbus.Event;
import no.uib.inf101.v23.lecture17.personlistviewer.model.Person;

/**
 * An event that is posted when a person is clicked. Contains the person
 * that was clicked.
 */
public record PersonClickedEvent(Person personClicked) implements Event { }
