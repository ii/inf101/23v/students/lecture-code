package no.uib.inf101.v23.lecture17.personlistviewer.view;

import no.uib.inf101.v23.lecture17.personlistviewer.eventbus.Event;

/**
 * An event that is posted when a person is created. Contains the name
 * and age of the person.
 */
public record PersonCreatedEvent(String name, int age) implements Event { }
