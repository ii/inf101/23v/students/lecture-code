package no.uib.inf101.v23.lecture17.personlistviewer.controller;


import no.uib.inf101.v23.lecture17.personlistviewer.eventbus.Event;
import no.uib.inf101.v23.lecture17.personlistviewer.eventbus.EventBus;
import no.uib.inf101.v23.lecture17.personlistviewer.model.Person;
import no.uib.inf101.v23.lecture17.personlistviewer.model.PersonList;
import no.uib.inf101.v23.lecture17.personlistviewer.view.PersonCreatedEvent;

/**
* A controller that adds a person to the model when a PersonCreatedEvent is
* posted to the event bus.
*/
public class AddPersonController {
  
  private final PersonList model;
  
  /**
  * Create a new controller that adds a person to the model when a
  * PersonCreatedEvent is posted to the event bus.
  *
  * @param model    The model to add the person to
  * @param eventBus The event bus to listen to
  */
  public AddPersonController(PersonList model, EventBus eventBus) {
    this.model = model;
    eventBus.register(this::reactToPersonCreatedEvents);
  }
  
  private void reactToPersonCreatedEvents(Event e) {
    if (e instanceof PersonCreatedEvent event) {
      Person newPerson = new Person(event.name(), event.age());
      this.model.addPerson(newPerson);
    }
  }
  
}
