package no.uib.inf101.v23.lecture17.personlistviewer;

import no.uib.inf101.v23.lecture17.personlistviewer.eventbus.EventBus;
import no.uib.inf101.v23.lecture17.personlistviewer.model.PersonList;
import no.uib.inf101.v23.lecture17.personlistviewer.view.ViewMain;
import no.uib.inf101.v23.lecture17.personlistviewer.controller.AddPersonController;

import javax.swing.JFrame;


/**
 * Start the application. Creates the event bus, model, view and
 * controller, and shows the view in a window.
 */
public class Main {

  /**
   * Start the application.
   *
   * @param args command line arguments (are ignored)
   */
  public static void main(String[] args) {
    EventBus eventBus = new EventBus();
    PersonList model = new PersonList(eventBus);
    ViewMain view = new ViewMain(model, eventBus);
    new AddPersonController(model, eventBus);

    JFrame frame = new JFrame("INF101 Person List");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setContentPane(view.getMainPanel());
    frame.pack();
    frame.setVisible(true);
  }
}
