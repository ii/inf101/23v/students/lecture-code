package no.uib.inf101.v23.lecture17.personlistviewer.eventbus;

@FunctionalInterface
public interface EventHandler {
  /**
   * Handle an event.
   * @param event the event to handle
   */
  void handle(Event event);
}
