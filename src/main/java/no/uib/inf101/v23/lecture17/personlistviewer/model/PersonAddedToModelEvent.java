package no.uib.inf101.v23.lecture17.personlistviewer.model;

import no.uib.inf101.v23.lecture17.personlistviewer.eventbus.Event;

/**
 * An event indicating that a person is added to the model.
 * Event contains the person in question.
 */
public record PersonAddedToModelEvent(Person person) implements Event { }
