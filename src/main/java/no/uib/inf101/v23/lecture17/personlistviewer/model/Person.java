package no.uib.inf101.v23.lecture17.personlistviewer.model;

/**
 * A person. Contains a name and an age.
 */
public record Person(String name, int age) { }
