package no.uib.inf101.v23.lecture17.fizzBuzz;

public class FizzBuzz {
 
    static String convert(int number) {
        if (number % 3 == 0) {
            return "Fizz";
        }
        if (number % 5 == 0) {
            return "Buzz";
        }
        if (number % 3 == 0 && number % 5 == 0) {
            return "FizzBuzz";
        }

        return number + "";
    }
    
}
