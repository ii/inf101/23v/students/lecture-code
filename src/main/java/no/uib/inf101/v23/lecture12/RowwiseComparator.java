package no.uib.inf101.v23.lecture12;

import java.util.Comparator;

public class RowwiseComparator implements Comparator<CellPosition> {

	@Override
	public int compare(CellPosition o1, CellPosition o2) {
		int cmp = Integer.compare(o1.row(), o2.row());
		if(cmp==0)
			cmp = Integer.compare(o1.col(), o2.col());
		return cmp;
	}

}
