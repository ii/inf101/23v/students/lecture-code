package no.uib.inf101.v23.lecture12;

public class UseTrafficLight {

	public static void main(String[] args) {
		TrafficLight light = new TrafficLight();
		System.out.println(light.greenIsOn()+" and "+light.redIsOn());
		while(!light.greenIsOn()) {
			light.next();
		}
		System.out.println(light.greenIsOn()+" and "+light.redIsOn());
	}

}
