package no.uib.inf101.v23.lecture9inheritance.animals;

public class Main {
  public static void main(String[] args) {
    Mammal man = new Mammal("Adam");
    Feline kitty = new Feline("Kitty");
    Mammal simba = new Lion("Simba");

    eatSpeakDrink(man);
    eatSpeakDrink(kitty);
    eatSpeakDrink(simba);
  }

  public static void eatSpeakDrink(Mammal mammal) {
    mammal.eat("meat");
    mammal.eat("veggie");
    mammal.speak();
    mammal.drinkMilk();
  }
}
