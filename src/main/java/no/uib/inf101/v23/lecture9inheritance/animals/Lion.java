package no.uib.inf101.v23.lecture9inheritance.animals;

public class Lion extends Feline {
  public Lion(String name) {
    super(name);
  }

  @Override
  public void speak() {
    System.out.println(this.name + ": ROAR!");
  }

  public void printKake() {
    System.out.println("kake");
  }
}
