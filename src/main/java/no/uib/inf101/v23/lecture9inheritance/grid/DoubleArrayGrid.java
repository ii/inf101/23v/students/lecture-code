package no.uib.inf101.v23.lecture9inheritance.grid;

import java.util.List;

public class DoubleArrayGrid implements BasicDoubleGrid {

  /* ... */

  @Override
  public void set(int row, int col, Double value) {
    /* ... */
  }

  @Override
  public Double get(int row, int col) {
    /* ... */
    return null;
  }

  @Override
  public int rows() {
    /* ... */
    return 0;
  }

  @Override
  public int cols() {
    /* ... */
    return 0;
  }

  @Override
  public List<Double> getDoubles() {
    return null;
  }
}
