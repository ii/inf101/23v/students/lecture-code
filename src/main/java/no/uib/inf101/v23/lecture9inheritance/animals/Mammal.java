package no.uib.inf101.v23.lecture9inheritance.animals;

public class Mammal {

  protected final String name;

  public Mammal(String name) {
    this.name = name;
  }

  public void eat(String food) {
    System.out.println(this.name + ": Yum, this " + food + " is delicious!");
  }

  public void speak() {
    System.out.println(this.name + ": (sits in silence)");
  }

  public void drinkMilk() {
    System.out.println(this.name + ": Mmmmm, milk!");
  }
}