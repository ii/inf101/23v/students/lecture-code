package no.uib.inf101.v23.lecture9inheritance.grid;

import java.util.List;

public interface MyGridInterface {

  Double getValue(int row, int col);

  void setValue(int row, int col, Double newValue);

  int rows();

  int cols();

  List<Double> getRow(int row);

}
