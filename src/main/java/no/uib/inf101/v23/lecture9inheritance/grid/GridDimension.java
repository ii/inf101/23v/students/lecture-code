package no.uib.inf101.v23.lecture9inheritance.grid;

public interface GridDimension {

  int rows();

  int cols();
}
