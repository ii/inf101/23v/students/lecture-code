package no.uib.inf101.v23.lecture10;

public class Box<T> {
    
    T item;
    
    public void placeItem(T item) {
        this.item = item;
    } 

    public T removeItem() {
        T temp = this.item;
        this.item = null;
        return temp;
    }

    public T peak() {
        return this.item;
    }

}
