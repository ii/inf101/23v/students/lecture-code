package no.uib.inf101.v23.lecture13mutability;

import java.util.Arrays;
import java.util.Objects;

public class Equality {
  public static void main(String[] args) {
    mutation();
    arrayEquality();
    objectArrayEquality();
    deepEquality();
  }

  private static void mutation() {
    int a = 2;
    int b = 4;
    int c = 3;

    int[] myNums = {a, b, c};
    int[] yourNums = {a, b, c};
    int[] theirNums = yourNums;

    a = 102;
    myNums[1] = 104;
    yourNums[2] = 103;

    System.out.println(a + " " + b + " " + c);
    System.out.println(Arrays.toString(myNums));
    System.out.println(Arrays.toString(yourNums));
    System.out.println(Arrays.toString(theirNums));
  }

  private static void arrayEquality() {
    int a = 1002;
    int b = 1004;
    int c = 1003;

    int[] myNums = {a, b, c};
    int[] yourNums = {a, b, c};
    int[] theirNums = yourNums;

    System.out.println(myNums == yourNums);
    System.out.println(myNums == theirNums);
    System.out.println(yourNums == theirNums);

    System.out.println(myNums.equals(yourNums));
    System.out.println(myNums.equals(theirNums));
    System.out.println(yourNums.equals(theirNums));

    System.out.println(arraysAreEqual(myNums, yourNums)); // true
  }

  private static boolean arraysAreEqual(int[] arrA, int[] arrB) {
    if (arrA == arrB) {
      return true;
    }
    if (arrA == null || arrB == null) {
      return false;
    }
    if (arrA.length != arrB.length) {
      return false;
    }

    // Hvis vi er her, så er listene like lange
    for (int i = 0; i < arrA.length; i++) {
      if (arrA[i] != arrB[i]) {
        return false;
      }
    }
    return true;
  }

  private static void objectArrayEquality() {
    Integer a = 1002;
    Integer b = 1004;
    Integer c = 1003;

    Integer[] myNums = {a, b, c};
    Integer[] yourNums = {a, b, c};
    Integer[] theirNums = yourNums;

    System.out.println(myNums == yourNums);
    System.out.println(myNums == theirNums);
    System.out.println(yourNums == theirNums);

    System.out.println(myNums.equals(yourNums));
    System.out.println(myNums.equals(theirNums));
    System.out.println(yourNums.equals(theirNums));

    System.out.println(arraysAreEqual(myNums, yourNums));
    System.out.println(arraysAreEqual(myNums, theirNums));
    System.out.println(arraysAreEqual(yourNums, theirNums));
  }

  private static boolean arraysAreEqual(Object[] arrA, Object[] arrB) {
    if (arrA == arrB) {
      return true;
    }
    if (arrA == null || arrB == null) {
      return false;
    }
    if (arrA.length != arrB.length) {
      return false;
    }
    // Hvis vi er her, så er listene like lange
    for (int i = 0; i < arrA.length; i++) {
      if (!Objects.equals(arrA[i], arrB[i])) {
        return false;
      }
    }
    return true;
  }


  private static void deepEquality() {
    Boolean[][] myTetrisPiece = new Boolean[][] {
        {  true,  true,  true },
        { false,  true, false }
    };
    Boolean[][] yourTetrisPiece = new Boolean[][] {
        {  true,  true,  true },
        { false,  true, false }
    };

    System.out.println(arraysAreEqual(myTetrisPiece, yourTetrisPiece));
    System.out.println(arraysAreDeepEqual(myTetrisPiece, yourTetrisPiece));
  }


  private static boolean arraysAreDeepEqual(Object[] arrA, Object[] arrB) {
    if (arrA == arrB) return true;
    if (arrA == null || arrB == null) return false;
    if (arrA.length != arrB.length) return false;

    for (int i = 0; i < arrA.length; i++) {
      Object ai = arrA[i];
      Object bi = arrB[i];

      if (ai instanceof Object[] && bi instanceof Object[]) {
        Object[] ai_arr = (Object[]) ai;
        Object[] bi_arr = (Object[]) bi;

        // Kode som sammenligner om to arrayer ai og bi er like
        if (!arraysAreDeepEqual(ai_arr, bi_arr)) {
          return false;
        }
      }
      else if (!Objects.equals(ai, bi)) {
        return  false;
      }
    }
    return true;
  }

}
