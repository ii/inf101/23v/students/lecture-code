package no.uib.inf101.v23.lecture11;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class UseCollections {

	public static void main(String[] args) {
		//ArrayList<Integer> alist = new ArrayList<Integer>();
		List<Integer> alist = new LinkedList<Integer>();
		fillCollection(alist, 10, 101);
		System.out.println(alist);
		alist.add(101);
		System.out.println(alist);
		System.out.println(alist.get(3)); //tabell[3]
		alist.set(3, 42);
		System.out.println(alist);
		
		Collections.reverse(alist);
		System.out.println(alist);
		Collections.sort(alist);
		System.out.println(alist);
		
		HashSet<Integer> set = new HashSet<Integer>();
		fillCollection(set, 10, 101);
		System.out.println(set);
		for(int i : set) {
			System.out.println(i+" modulo 16 = "+ i%16);
		}
	}

	static void fillCollection(Collection<Integer> coll, int n, int max){
		long seed = 1031010167;
		Random rand = new Random(seed);
		for(int i=0; i<n; i++) {
			coll.add(rand.nextInt(max));
		}
	}
}
