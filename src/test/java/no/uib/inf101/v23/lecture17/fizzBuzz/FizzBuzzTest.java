package no.uib.inf101.v23.lecture17.fizzBuzz;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class FizzBuzzTest {
    
    @Test
    public void oneConvertsToOne() {
        int input = 1;
        String expected = "1";
        String actual = FizzBuzz.convert(input);

        assertEquals(expected, actual);
    }

    @Test
    public void twoConvertsToTwo() {
        int input = 2;
        String expected = "2";
        String actual = FizzBuzz.convert(input);

        assertEquals(expected, actual);
    }

    @Test
    public void threeConvertsToFizz() {
        int input = 3;
        String expected = "Fizz";
        String actual = FizzBuzz.convert(input);

        assertEquals(expected, actual);
    }

    @Test
    public void fiveConvertsToBuzz() {
        int input = 5;
        String expected = "Buzz";
        String actual = FizzBuzz.convert(input);

        assertEquals(expected, actual);
    }

    @Test
    public void fifteenConvertsToFizzBuzz() {
        int input = 15;
        String expected = "FizzBuzz";
        String actual = FizzBuzz.convert(input);

        assertEquals(expected, actual);
    }
}
