package no.uib.inf101.v23.lecture17.pokemon;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class PokemonTest {
    
    @Test
    public void getNameTest() {
        String name = "Mew";
        Pokemon pokemon = new Pokemon(name);

        assertEquals(name, pokemon.getName());
    }

    @Test
    public void pokemonIsAliveWhenCreated() {
        String name = "Mew";
        Pokemon pokemon = new Pokemon(name);

        assertTrue(pokemon.isAlive());
    }

    @Test
    public void nullNamePokemonThrowsException() {
        String name = null;
        assertThrows(IllegalArgumentException.class, () -> {
            new Pokemon(name);
        });
    }

    @Test
    public void emptyNamePokemonThrowsException() {
        String name = "";
        assertThrows(IllegalArgumentException.class, () -> {
            new Pokemon(name);
        });
    }

}
