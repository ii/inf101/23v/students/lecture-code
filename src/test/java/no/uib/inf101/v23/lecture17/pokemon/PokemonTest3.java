package no.uib.inf101.v23.lecture17.pokemon;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.Test;

public class PokemonTest3 {
    
    static IPokemon pokemon = new Pokemon("Mew");

    @Test
    public void pokemonIsDefeatedAfterDamage() {
        int currentHealth = pokemon.getCurrentHP();
        pokemon.damage(currentHealth);

        assertFalse(pokemon.isAlive());
    }

    @Test
    public void pokemonTakesNoDamageAfterDeath() {
        int currentHealth = pokemon.getCurrentHP();
        pokemon.damage(10);

        assertEquals(currentHealth, pokemon.getCurrentHP());
    }
    
}
