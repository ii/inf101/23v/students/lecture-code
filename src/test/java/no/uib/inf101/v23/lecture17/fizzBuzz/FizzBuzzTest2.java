package no.uib.inf101.v23.lecture17.fizzBuzz;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

public class FizzBuzzTest2 {
    
    @Test
    public void nonFizzBuzzNumbers() {
        for (int i = 1; i <= 100; i++) {
            if (i % 3 == 0)
                continue;
            if (i % 5 == 0)
                continue;
            String actual = FizzBuzz.convert(i);
            String expected = i + "";
            
            assertEquals(expected, actual);
        }
    }

    @Test
    public void knownFizzNumbers() {
        List<Integer> knownFizzNumbers = Arrays.asList(3, 6, 9, 12, 18, 21, 24, 27, 33, 36, 39);
        for (int number : knownFizzNumbers) {
            String actual = FizzBuzz.convert(number);
            
            assertEquals("Fizz", actual);
        }
    }




    @Test
    public void checkGeneralBehavior() {
        for (int i = 1; i <= 100; i++) {
            String expected;
            if (i % 3 == 0) {
                expected = "Fizz";
            }
            else if (i % 5 == 0) {
                expected = "Buzz";
            }
            else if (i % 3 == 0 && i % 5 == 0) {
                expected = "FizzBuzz";
            }
            else {
                expected = i + "";
            }
            String actual = FizzBuzz.convert(i);

            assertEquals(expected, actual);
        }
    }
















    

    @Test
    public void oneConvertsToOne() {
        int input = 1;
        String expected = "1";
        String actual = FizzBuzz.convert(input);

        assertEquals(expected, actual);
    }

    @Test
    public void twoConvertsToTwo() {
        int input = 2;
        String expected = "2";
        String actual = FizzBuzz.convert(input);

        assertEquals(expected, actual);
    }

    @Test
    public void threeConvertsToFizz() {
        int input = 3;
        String expected = "Fizz";
        String actual = FizzBuzz.convert(input);

        assertEquals(expected, actual);
    }

    @Test
    public void fiveConvertsToBuzz() {
        int input = 5;
        String expected = "Buzz";
        String actual = FizzBuzz.convert(input);

        assertEquals(expected, actual);
    }

    @Test
    public void fifteenConvertsToFizzBuzz() {
        int input = 15;
        String expected = "FizzBuzz";
        String actual = FizzBuzz.convert(input);

        assertEquals(expected, actual);
    }

}
